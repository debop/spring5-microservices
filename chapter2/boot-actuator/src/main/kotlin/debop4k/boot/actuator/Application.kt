/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.boot.actuator

import mu.KLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.boot.actuate.metrics.CounterService
import org.springframework.boot.actuate.metrics.GaugeService
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.*
import java.util.*
import java.util.concurrent.atomic.*

/**
 * debop4k.boot.actuator.Application
 * @author debop
 * @since 17. 10. 24
 */
@SpringBootApplication
class Application {
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}

class TPSCounter {
  val count = LongAdder()
  val threshold = 2
  val expiry = Calendar.getInstance().apply { add(Calendar.MINUTE, 1) }

  fun isExpired(): Boolean = Calendar.getInstance().after(expiry)
  fun isWeak(): Boolean = count.toInt() > threshold
  fun increment() {
    count.increment()
  }
}

@Component
class TPSHealth : HealthIndicator {

  var counter: TPSCounter? = null

  override fun health(): Health {
    val health = howGoodIsHealth()
    if (health) {
      return Health.outOfService().withDetail("Too many requests", "OutofService").build()
    }
    return Health.up().build()
  }

  fun updateTx() {
    if (counter?.isExpired() ?: true) {
      counter = TPSCounter()
    }
    counter!!.increment()
  }

  fun howGoodIsHealth(): Boolean = counter?.isWeak() ?: false

}

// Spring Boot 에서 제공하는 기본적인 CounterService, GaugeService 등은 Injection을 받아 사용하면 된다.
// Dropwizard metrics 의 annotation을 사용하는 방식은 기존 코드를 건드릴 필요가 없다는 장점이 있다.
//
@RestController
class GreetingController @Autowired constructor(val health: TPSHealth,
                                                val counterService: CounterService,
                                                val gaugeService: GaugeService) {
  companion object : KLogging()

  @CrossOrigin
  @RequestMapping("/")
  fun greet(): Greet {
    logger.info { "Serving Request ... " }
    health.updateTx()
    counterService.increment("greet.txnCount")
    gaugeService.submit("greet.customgauge", 1.0)

    return Greet("Hello World!")
  }
}

data class Greet @JvmOverloads constructor(var message: String = "") {
  override fun toString(): String = message
}