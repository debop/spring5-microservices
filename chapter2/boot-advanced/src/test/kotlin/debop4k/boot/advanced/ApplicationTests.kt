/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.boot.advanced

import org.junit.*
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod.GET
import org.springframework.security.crypto.codec.Base64
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.client.RestTemplate


@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(Application::class), webEnvironment = DEFINED_PORT)
class ApplicationTests {

  @Ignore("보안 기능이 있어서 직접 접속할 수 없다")
  @Test
  fun testVanillaService() {
    val restTemplate = RestTemplate()
    val greet = restTemplate.getForObject("http://localhost:8080", Greet::class.java)
    Assert.assertEquals("Hello World!", greet.message)
  }

  @Ignore("Basic Authorization 에 대한 테스트로서 oAutho관련 기능이 없어야 가능합니다.")
  @Test
  fun testSecureService() {
    val plainCreds = "guest:guest123"
    val headers = HttpHeaders().apply {
      add("Authorization", "Basic " + String(Base64.encode(plainCreds.toByteArray())))
    }
    val request = HttpEntity<String>(headers)
    val restTemplate = RestTemplate()

    val response = restTemplate.exchange("http://localhost:8080", GET, request, Greet::class.java)
    Assert.assertEquals("Hello World!", response.body.message)
  }


  @Test
  fun testOAuthService() {
    val resource = ResourceOwnerPasswordResourceDetails().apply {
      username = "guest"
      password = "guest123"
      accessTokenUri = "http://localhost:8080/oauth/token"
      clientId = "trustedClient"
      clientSecret = "trustedClient123"
      grantType = "password"
    }

    val clientContext = DefaultOAuth2ClientContext()
    val restTemplate = OAuth2RestTemplate(resource, clientContext)

    val greet = restTemplate.getForObject("http://localhost:8080", Greet::class.java)
    Assert.assertEquals("Hello World!", greet.message)
  }
}