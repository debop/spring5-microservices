package debop4k.boot.advanced

import mu.KLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.core.env.Environment
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.web.bind.annotation.*

// Basic Authorization 사용 시
// @EnableGlobalMethodSecurity

// OAuth2 사용 시
@EnableResourceServer
@EnableAuthorizationServer
@EnableGlobalMethodSecurity
@SpringBootApplication
class Application {
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}

@RestController
class GreetingController @Autowired constructor(val env: Environment) {

  companion object : KLogging()

  @CrossOrigin
  @RequestMapping("/")
  fun greet(): Greet {
    logger.info { "bootrest.customproperty: ${env.getProperty("bootrest.customproperty")}" }
    return Greet("Hello World!")
  }
}

data class Greet @JvmOverloads constructor(var message: String = "") {

  override fun toString(): String = message

}