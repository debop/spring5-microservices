# boot-messaging

RabbitMQ 를 이용하여, message 를 전송합니다.
이 프로젝트를 실행하려면 RabbitMQ 를 설치하고 실행해야 합니다.

## RabbitMQ 설치

Docker 를 이용해 RabbitMQ 를 설치합니다.

    # RabbitMQ 최신 Image 를 다운로드 받습니다.
    $ sudo docker pull rabbitmq:latest
    
    # RabbitMQ Image 로부터 `rabbitmq-instance` container 를 만들고 실행합니다. 
    $ sudo docker run -D --name rabbitmq-instance -p 5672:5672 rabbitmq
    
    # 만약 기존 container 를 실행하려면 다음과 같이 하면 됩니다.
    $ sudo docker start rabbitmq-instance
    
    # RabbitMQ container를 중지합니다.
    $ sudo docker stop rabbitmq-instance
    
    

      