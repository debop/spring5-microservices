/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.boot.messaging

import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

/**
 * debop4k.boot.messaging.Application
 * @author debop
 * @since 17. 10. 24
 */
@SpringBootApplication
class Application : CommandLineRunner {

  @Autowired lateinit var sender: Sender

  override fun run(vararg args: String?) {
    sender.send("Hello Messaging..!!!")
  }
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}

@Component
class Sender @Autowired constructor(val template: RabbitMessagingTemplate) {

  @Bean fun queue(): Queue = Queue("TestQ", false)

  fun send(message: String) {
    template.convertAndSend("TestQ", message)
  }
}


@Component
class Receiver {
  @RabbitListener(queues = arrayOf("TestQ"))
  fun processMessage(content: String) {
    println("Received: $content")
  }
}

