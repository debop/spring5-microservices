/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.boot.customer

import mu.KLogging
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.test.context.junit4.SpringRunner

/**
 * debop4k.boot.customer.ApplicationTests
 * @author debop
 * @since 17. 10. 24
 */
@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(Application::class), webEnvironment = RANDOM_PORT)
class ApplicationTests {

  companion object : KLogging()

  @Autowired lateinit var registrar: CustomerRegistrar
  @Autowired lateinit var repository: CustomerRepository

  @Test
  fun contextLoads() {
    Assert.assertNotNull(registrar)
    Assert.assertNotNull(repository)
  }

  @Test
  fun loadCustomer() {
    val customers = repository.findAll()
    Assert.assertEquals(7, customers.size)

    customers.forEach {
      logger.debug { "Loaded customer=$it" }
    }
  }


}