/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.boot.customer

import mu.KLogging
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Lazy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.persistence.*

@SpringBootApplication
class Application {

  /**
   * Spring Boot Application 시작 시 실행되도록 합니다.
   */
  @Bean
  fun init(customerRepository: CustomerRepository): CommandLineRunner {
    return CommandLineRunner {
      with(customerRepository) {
        save(Customer("Adam", "adam@boot.com"))
        save(Customer("John", "john@boot.com"))
        save(Customer("Smith", "smith@boot.com"))
        save(Customer("Edgar", "edgar@boot.com"))
        save(Customer("Martin", "martin@boot.com"))
        save(Customer("Tom", "tom@boot.com"))
        save(Customer("Sean", "sean@boot.com"))
      }

    }
  }
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}


@RestController
class CustomerController @Autowired constructor(val registrar: CustomerRegistrar) {

  companion object : KLogging()

  @RequestMapping(path = arrayOf("/regstrar"), method = arrayOf(RequestMethod.POST))
  fun register(@RequestBody customer: Customer): Customer {
    logger.info { "Save request customer. customer=$customer" }
    return registrar.register(customer)
  }
}

@Component
@Lazy
class CustomerRegistrar @Autowired constructor(val repository: CustomerRepository,
                                               val sender: Sender) {

  companion object : KLogging()

  fun register(customer: Customer): Customer {
    val existingCustomer = repository.findByName(customer.name!!)
    if (existingCustomer.isPresent) {
      throw RuntimeException("$customer is already exists.")
    } else {
      logger.info { "Save new customer and send result message to MQ ..." }
      repository.save(customer)
      sender.send(customer.email!!)
    }
    return customer
  }
}

@Component
@Lazy
class Sender @Autowired constructor(val template: RabbitMessagingTemplate) {

  @Bean
  fun queue(): Queue = Queue("CustomerQ", false)

  fun send(message: String) {
    template.convertAndSend("CustomerQ", message)
  }

}

@RepositoryRestResource
@Lazy
interface CustomerRepository : JpaRepository<Customer, Long> {

  fun findByName(@Param("name") name: String): Optional<Customer>

}

@Entity
class Customer @JvmOverloads constructor(var name: String? = null,
                                         var email: String? = null) {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  var id: Long? = null

  override fun toString(): String = "Customer [id=$id, name=$name, email=$email]"
}