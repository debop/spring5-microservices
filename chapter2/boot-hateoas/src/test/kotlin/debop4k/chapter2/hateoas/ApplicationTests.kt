/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.chapter2.hateoas

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit4.SpringRunner

/**
 * debop4k.chapter2.hateoas.ApplicationTests
 * @author debop
 * @since 17. 10. 24
 */
@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(Application::class), webEnvironment = RANDOM_PORT)
class ApplicationTests {

  @Autowired lateinit var restTemplate: TestRestTemplate

  @Test
  fun getGreet() {
    val greet: Greet = restTemplate.getForObject("/greet", Greet::class.java)
    Assert.assertNotNull(greet)
    Assert.assertEquals("Hello World!", greet.message)
  }

  @Test
  fun getGreeting() {
    val entity: ResponseEntity<Greet> = restTemplate.getForEntity("/greeting?name={name}", Greet::class.java, mapOf("name" to "debop"))
    Assert.assertEquals(HttpStatus.OK, entity.statusCode)
    Assert.assertTrue(entity.hasBody())

    val greet = entity.body
    Assert.assertEquals("Hello debop", greet.message)
  }
}