/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.chapter2.hateoas

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.hateoas.ResourceSupport
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn
import org.springframework.http.*
import org.springframework.web.bind.annotation.*

/**
 * debop4k.chapter2.hateoas.Application
 * @author debop
 * @since 17. 10. 24
 */
@SpringBootApplication
class Application {
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}

@RestController
open class GreetingController {

  @RequestMapping("/greet")
  fun greet(): Greet = Greet("Hello World!")

  @RequestMapping("/greeting")
  @ResponseBody
  fun greeting(@RequestParam(value = "name", required = false, defaultValue = "HATEOAS") name: String): HttpEntity<Greet> {
    val greet = Greet("Hello " + name).apply {
      // ControllerLinkBuilder.linkTo
      // ControllerLinkBuilder.methodOn
      // 를 이용하여, 자신의 Link 를 표현한다.
      /*
      {
        "message": "Hello debop",
        "_links": {
          "self": {
            "href": "http://localhost:8080/greeting?name=debop"
          }
        }
      }
      */
      add(linkTo(methodOn(GreetingController::class.java).greeting(name)).withSelfRel())

      //
      /*
      {
        "message": "Hello debop",
        "_links": {
          "self": {
            "href": "http://localhost:8080/greeting?name=debop"
          }
          "greet": {
            "href": "http://localhost:8080/greet"
          }
        }
      }
       */
      add(linkTo(methodOn(GreetingController::class.java).greet()).withRel("greet"))
    }
    return ResponseEntity(greet, HttpStatus.OK)
  }
}

// Hateoas 를 지원하기 위해 `ResourceSupport` 를 상속받는다.
// 위의 GreetingController#greet 메소드를 HATEOAS 로 link 를 추가하려면 Greet 클래스가 open class 이어야 합니다.
//
open class Greet @JvmOverloads constructor(var message: String = "") : ResourceSupport() {

  override fun toString(): String = message
}