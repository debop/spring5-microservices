/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package debop4k.boot.swagger

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.web.bind.annotation.*
import springfox.documentation.swagger2.annotations.EnableSwagger2

// Swagger2 UI 를 보려면 http://localhost:8080/swagger-ui.html 에 접속하시면 됩니다.   
//
@SpringBootApplication
@EnableSwagger2
class Swagger2SpringBoot {
}

fun main(vararg args: String) {
  SpringApplication.run(Swagger2SpringBoot::class.java, *args)
}

@RestController
class GreetController {

  @RequestMapping("/", method = arrayOf(RequestMethod.GET))
  fun greet(): String {
    return "Hello World!!"
  }
}