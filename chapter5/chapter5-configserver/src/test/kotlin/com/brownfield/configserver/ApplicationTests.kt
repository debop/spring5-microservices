/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brownfield.configserver

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.junit4.SpringRunner
import javax.inject.Inject

/**
 * com.brownfield.configserver.ApplicationTests
 * @author debop
 * @since 17. 10. 27
 */
@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(Application::class))
class ApplicationTests {

  @Inject lateinit var appContext: ApplicationContext

  @Test fun contextLoads() {
    Assert.assertNotNull(appContext)
  }
}