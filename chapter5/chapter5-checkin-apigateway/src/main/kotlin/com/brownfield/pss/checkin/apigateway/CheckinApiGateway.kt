/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brownfield.pss.checkin.apigateway

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.zuul.EnableZuulProxy
import springfox.documentation.swagger2.annotations.EnableSwagger2

// bootstrap.properties 에 server.port, spring.cloud.config.url 등을 정의해주어야 한다.
//
@EnableZuulProxy
@EnableDiscoveryClient
@EnableSwagger2
@SpringBootApplication
class CheckinApiGateway {
}

fun main(vararg args: String) {
  SpringApplication.run(CheckinApiGateway::class.java, *args)
}