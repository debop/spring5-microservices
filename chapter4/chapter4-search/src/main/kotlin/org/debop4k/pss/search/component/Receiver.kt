/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.search.component

import mu.KLogging
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

/**
 * RabbitMQ Receiver
 *
 * @author debop
 * @since 17. 10. 24
 */
@Component
class Receiver @Autowired constructor(private val searchComponent: SearchComponent) {

  companion object : KLogging() {
    const val SEARCH_QUEUE_NAME = "SearchQ"
  }

  @Bean fun queue(): Queue = Queue(SEARCH_QUEUE_NAME, false)

  @RabbitListener(queues = arrayOf(SEARCH_QUEUE_NAME))
  fun processMessage(fare: Map<String, Any?>) {
    logger.info { "Received: $fare" }

    searchComponent.updateInventory(fare["FLIGHT_NUMBER"].toString(),
                                    fare["FLIGHT_DATE"].toString(),
                                    fare["NEW_INVENTORY"]?.toString()?.toInt() ?: 0)
  }


}