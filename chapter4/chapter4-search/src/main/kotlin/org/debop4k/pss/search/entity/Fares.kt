/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.search.entity

import javax.persistence.*
import javax.persistence.GenerationType.IDENTITY

@Entity
class Fares @JvmOverloads constructor(var fare: String = "",
                                      var currency: String = "") {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "fare_id")
  var id: Long = 0


  override fun toString(): String =
      "Fares [id=$id, fare=$fare, currency=$currency]"
}