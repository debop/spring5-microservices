/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.search.component

import mu.KLogging
import org.debop4k.pss.search.controller.SearchQuery
import org.debop4k.pss.search.entity.Flight
import org.debop4k.pss.search.repository.FlightRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * org.debop4k.pss.search.component.SearchComponent
 * @author debop
 * @since 17. 10. 24
 */
@Component
class SearchComponent @Autowired constructor(private val flightRepository: FlightRepository) {

  companion object : KLogging()

  fun search(query: SearchQuery): List<Flight> {
    val flights = flightRepository.findByOriginAndDestinationAndFlightDate(query.origin,
                                                                           query.destination,
                                                                           query.flightDate)
    return flights
        .onEach { it.fares }  // one-to-one lazy initialize 일 수 있으므로 
        .filter { it.inventory?.count ?: -1 >= 0 }.toList()
  }

  fun updateInventory(flightNumber: String, flightDate: String, inventory: Int) {
    logger.info { "Update inventory for flight=$flightNumber, inventory=$inventory" }

    val flight = flightRepository.findByFlightNumberAndFlightDate(flightNumber, flightDate)
    flight?.inventory?.count = inventory

    flightRepository.save(flight)
  }
}