/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.search

import mu.KLogging
import org.debop4k.pss.search.entity.*
import org.debop4k.pss.search.repository.FlightRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * org.debop4k.pss.search.Application
 * @author debop
 * @since 17. 10. 24
 */
@SpringBootApplication
class Application : CommandLineRunner {

  companion object : KLogging()

  @Autowired lateinit var flightRepo: FlightRepository

  override fun run(vararg args: String?) {

    val flights = listOf(Flight("BF100", "SEA", "SFO", "2016-01-22", Fares("100", "USD"), Inventory(100)),
                         Flight("BF101", "NYC", "SFO", "2016-01-22", Fares("101", "USD"), Inventory(100)),
                         Flight("BF105", "NYC", "SFO", "2016-01-22", Fares("105", "USD"), Inventory(100)),
                         Flight("BF106", "NYC", "SFO", "2016-01-22", Fares("106", "USD"), Inventory(100)),
                         Flight("BF102", "CHI", "SFO", "2016-01-22", Fares("102", "USD"), Inventory(100)),
                         Flight("BF103", "HOU", "SFO", "2016-01-22", Fares("103", "USD"), Inventory(100)),
                         Flight("BF104", "LAX", "SFO", "2016-01-22", Fares("104", "USD"), Inventory(100)))

    flightRepo.save(flights)

    logger.info { "Looking to load flights..." }
    flightRepo.findByOriginAndDestinationAndFlightDate("NYC", "SFO", "2016-01-22").forEach {
      logger.info { it }
    }
  }
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}