/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.book.component

import mu.KLogging
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Service
import javax.inject.Inject

/**
 * org.debop4k.pss.book.component.Sender
 * @author debop
 * @since 17. 10. 26
 */
@Service
class Sender @Inject constructor(private val template: RabbitMessagingTemplate) {

  companion object : KLogging()

  @Bean fun searchQueue(): Queue = Queue("SearchQ", false)

  @Bean fun checkInQueue(): Queue = Queue("CheckINQ", false)


  fun send(message: Any?) {
    message?.let {
      logger.debug { "Send message to search queue. queue=SearchQ, message=$message" }
      template.convertAndSend("SearchQ", message)
    }
  }

}