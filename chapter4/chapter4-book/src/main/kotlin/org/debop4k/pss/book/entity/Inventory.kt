/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.book.entity

import java.io.Serializable
import javax.persistence.*

/**
 * org.debop4k.pss.book.entity.Inventory
 * @author debop
 * @since 17. 10. 25
 */
@Entity
class Inventory @JvmOverloads constructor(var flightNumber: String = "",
                                          var flightDate: String = "",
                                          var available: Int = 0) : Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  var id: Long = 0

  /** 좌석 여유가 있는지 확인 */
  fun isAvailable(passengerSize: Int): Boolean = (available - passengerSize) > 5

  val bookableInventory: Int get() = available - 5

  override fun toString(): String =
      "Inventory [id=$id, flightNumber=$flightNumber, flightDate=$flightDate, available=$available]"
}