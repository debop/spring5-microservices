/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.book.controller

import mu.KLogging
import org.debop4k.pss.book.component.BookingComponent
import org.debop4k.pss.book.entity.BookingRecord
import org.springframework.web.bind.annotation.*
import javax.inject.Inject

/**
 * org.debop4k.pss.book.controller.BookingController
 * @author debop
 * @since 17. 10. 26
 */
@RestController
@CrossOrigin
@RequestMapping("/booking")
class BookingController @Inject constructor(private val bookingComponent: BookingComponent) {

  companion object : KLogging()

  @PostMapping("/create")
  fun book(@RequestBody record: BookingRecord): Long {
    logger.info { "Booking Request=$record" }
    return bookingComponent.book(record)
  }

  @GetMapping("/get/{id}")
  fun getBooking(@PathVariable id: Long): BookingRecord?
      = bookingComponent.getBooking(id)

}