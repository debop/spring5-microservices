/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.book.entity

import java.io.Serializable
import java.util.*
import javax.persistence.*
import javax.persistence.CascadeType.ALL
import javax.persistence.FetchType.EAGER
import javax.persistence.GenerationType.IDENTITY

/**
 * org.debop4k.pss.book.entity.BookingRecord
 * @author debop
 * @since 17. 10. 25
 */
@Entity
class BookingRecord @JvmOverloads constructor(var flightNumber: String = "",
                                              var from: String = "",
                                              var to: String = "",
                                              var flightDate: String = "",
                                              var bookingDate: Date = Date(),
                                              var fare: String = "",
                                              var status: String = "") : Serializable {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  var id: Long = 0

  @OneToMany(fetch = EAGER, cascade = arrayOf(ALL), mappedBy = "bookingRecord")
  val passengers: HashSet<Passenger> = hashSetOf()

  fun addPassenger(passenger: Passenger) {
    passengers.add(passenger)
    passenger.bookingRecord = this
  }

  override fun toString(): String =
      "BookingRecord [id=$id, flughtNumber=$flightNumber, from=$from, to=$to, " +
      "bookingDate=$bookingDate, fare=$fare, status=$status, passengers=$passengers]"
}