/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.book.component

import mu.KLogging
import org.debop4k.pss.book.entity.BookingRecord
import org.debop4k.pss.book.repository.BookingRepository
import org.debop4k.pss.book.repository.InventoryRepository
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.util.*
import javax.inject.Inject


@Service
class BookingComponent @Inject constructor(val bookingRepository: BookingRepository,
                                           val sender: Sender,
                                           val inventoryRepository: InventoryRepository) {

  companion object : KLogging() {
    const val FareURL = "http://localhost:8080/fares"
  }

  private val restTemplate: RestTemplate = RestTemplate()

  fun getBooking(id: Long): BookingRecord? = bookingRepository.findOne(id)

  fun updateStatus(status: String, bookingId: Long) {
    getBooking(bookingId)?.let {
      it.status = status
      bookingRepository.save(it)
    }
  }

  /**
   * 예약을 수행합니다.
   *
   * 1. fare 조회
   * 2. inventory 가 있는지 확인한다.
   * 3. 예약이 되면 MQ 로 결과 메시지 전송
   */
  fun book(record: BookingRecord): Long {
    logger.info { "calling fares to get fare" }

    // get fare
    val fare = restTemplate.getForObject(FareURL + "/get?flightNumber=${record.flightNumber}&flightDate=${record.flightDate}", Fare::class.java)
    logger.info { "Received fare=$fare" }

    // check fare
    if (Objects.equals(record.fare, fare))
      throw BookingException("fare is tampered (fake)")

    logger.info { "calling inventory to get inventory" }
    val inventory = inventoryRepository.findByFlightNumberAndFlightDate(record.flightNumber, record.flightDate)
    check(inventory != null) { "Inventory not found." }

    if (!inventory!!.isAvailable(record.passengers.size)) {
      throw BookingException("No more seats available")
    }

    logger.info { "Successully checked inventory. $inventory" }
    logger.info { "calling inventory to update inventory." }

    inventory.available -= record.passengers.size
    inventoryRepository.saveAndFlush(inventory)

    logger.info { "Successfully update inventory." }

    // save booking
    record.status = BookingStatus.BOOKING_CONFIRMED
    val passengers = record.passengers
    passengers.forEach { it.bookingRecord = record }
    record.bookingDate = Date()
    val id = bookingRepository.save(record).id

    logger.info { "Successully save booking record. $record" }

    // Send a message to search to update inventory
    logger.info { "Sending a booking event" }

    val bookingDetails = hashMapOf<String, Any?>("FLIGHT_NUMBER" to record.flightNumber,
                                                 "FLIGHT_DATE" to record.flightDate,
                                                 "NEW_INVENTORY" to inventory.bookableInventory)

    sender.send(bookingDetails)

    logger.info { "booking event successfully delivered. $bookingDetails" }

    return id
  }
}