/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.book

import mu.KLogging
import org.debop4k.pss.book.component.BookingComponent
import org.debop4k.pss.book.entity.*
import org.debop4k.pss.book.repository.BookingRepository
import org.debop4k.pss.book.repository.InventoryRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.util.*
import javax.inject.Inject

/**
 * org.debop4k.pss.book.Application
 * @author debop
 * @since 17. 10. 25
 */
@SpringBootApplication
class Application : CommandLineRunner {

  companion object : KLogging()

  @Inject lateinit var bookingRepository: BookingRepository
  @Inject lateinit var bookingComponent: BookingComponent
  @Inject lateinit var inventoryRepository: InventoryRepository

  override fun run(vararg args: String?) {

    val inventories = listOf(Inventory("BF100", "2016-01-22", 100),
                             Inventory("BF101", "2016-01-22", 100),
                             Inventory("BF102", "2016-01-22", 100),
                             Inventory("BF103", "2016-01-22", 100),
                             Inventory("BF104", "2016-01-22", 100),
                             Inventory("BF105", "2016-01-22", 100),
                             Inventory("BF106", "2016-01-22", 100))
    inventoryRepository.save(inventories)

    val booking = BookingRecord("BF101", "NYC", "SFO", "2016-01-22", Date(), "101").apply {
      addPassenger(Passenger("Gean", "Franc", "Male", this))
    }

    val bookingId = bookingComponent.book(booking)
    logger.info { "Booking successfully saved ... $bookingId" }

    logger.info { "Looking to load booking record..." }
    logger.info { "Result: ${bookingComponent.getBooking(bookingId)}" }
  }
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}