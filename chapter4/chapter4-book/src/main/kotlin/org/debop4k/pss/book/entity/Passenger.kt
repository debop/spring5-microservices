/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.book.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import javax.persistence.*
import javax.persistence.FetchType.LAZY
import javax.persistence.GenerationType.IDENTITY

/**
 * org.debop4k.pss.book.entity.Passenger
 * @author debop
 * @since 17. 10. 25
 */
@Entity
class Passenger @JvmOverloads constructor(var firstName: String = "",
                                          var lastName: String = "",
                                          var gender: String = "",
                                          _bookingRecord: BookingRecord) : Serializable {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  var id: Long = 0

  @ManyToOne(fetch = LAZY)
  @JoinColumn(name = "booking_id")
  @JsonIgnore
  var bookingRecord: BookingRecord = _bookingRecord


  override fun toString(): String =
      "Passenger [id=$id, firstName=$firstName, lastName=$lastName, gender=$gender]"
}