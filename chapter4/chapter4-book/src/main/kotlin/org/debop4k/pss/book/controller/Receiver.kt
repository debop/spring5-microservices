/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.book.controller

import mu.KLogging
import org.debop4k.pss.book.component.BookingComponent
import org.debop4k.pss.book.component.BookingStatus
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import javax.inject.Inject

/**
 * org.debop4k.pss.book.controller.Receiver
 * @author debop
 * @since 17. 10. 26
 */
@Component
class Receiver @Inject constructor(private val bookingComponent: BookingComponent) {

  companion object : KLogging()

  @RabbitListener(queues = arrayOf("CheckINQ"))
  fun processMessage(bookingId: Long) {
    logger.info { "Received bookingId=$bookingId" }
    bookingComponent.updateStatus(BookingStatus.CHECKED_IN, bookingId)
  }
}