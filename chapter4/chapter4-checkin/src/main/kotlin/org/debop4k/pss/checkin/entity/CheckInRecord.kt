/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.checkin.entity

import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id


@Entity
data class CheckInRecord @JvmOverloads constructor(var lastName: String = "",
                                                   var firstName: String = "",
                                                   var seatNumber: String = "",
                                                   var checkInTime: Date = Date(),
                                                   var flightNumber: String = "",
                                                   var flightDate: String = "",
                                                   var bookingId: Long = 0) {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  var id: Long = 0


}