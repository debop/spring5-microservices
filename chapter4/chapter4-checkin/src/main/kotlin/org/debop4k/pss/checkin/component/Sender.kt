/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.checkin.component

import mu.KLogging
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class Sender @Autowired constructor(private val template: RabbitMessagingTemplate) {

  companion object : KLogging() {
    const val CHECK_IN_QUEUE_NAME = "CheckInQ"
  }

  @Bean fun queue(): Queue = Queue(CHECK_IN_QUEUE_NAME, false)

  fun send(message: Any) {
    logger.info { "Send message to $CHECK_IN_QUEUE_NAME. message=$message" }
    template.convertAndSend(CHECK_IN_QUEUE_NAME, message)
  }
}