/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.checkin

import mu.KLogging
import org.debop4k.pss.checkin.entity.CheckInRecord
import org.debop4k.pss.checkin.repository.CheckinRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.util.*

@SpringBootApplication
class Application : CommandLineRunner {

  companion object : KLogging()

  @Autowired lateinit var repository: CheckinRepository

  override fun run(vararg args: String?) {
    val record = CheckInRecord("Franc", "Gean", "28A", Date(), "BF101", "2016-01-22", 1)

    val result = repository.save(record)
    logger.info { "checked in successfully ... $result" }

    logger.info { "Looking to load checkedIn record..." }
    logger.info { "Result: ${repository.findOne(result.id)}" }
  }
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}