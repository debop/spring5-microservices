/*
 * Copyright (c) 2017. Sunghyouk Bae <sunghyouk.bae@gmail.com>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.checkin.controller

import mu.KLogging
import org.debop4k.pss.checkin.component.CheckinComponent
import org.debop4k.pss.checkin.entity.CheckInRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("/checkin")
class CheckInController @Autowired constructor(private val checkInComponent: CheckinComponent) {

  companion object : KLogging()

  @GetMapping("/get/{id}")
  fun getCheckIn(@PathVariable id: Long): CheckInRecord? {
    return checkInComponent.getCheckInRecord(id)
  }

  @PostMapping(value = "/create")
  fun checkIn(@RequestBody checkIn: CheckInRecord): Long {
    return checkInComponent.checkIn(checkIn)
  }


}