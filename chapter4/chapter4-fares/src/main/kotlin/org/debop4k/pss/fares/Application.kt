/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.fares

import mu.KLogging
import org.debop4k.pss.fares.entity.Fare
import org.debop4k.pss.fares.repository.FaresRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application : CommandLineRunner {

  companion object : KLogging()


  @Autowired lateinit var repository: FaresRepository

  override fun run(vararg args: String?) {
    val fares = listOf(Fare("BF100", "2016-01-22", "101"),
                       Fare("BF101", "2016-01-22", "101"),
                       Fare("BF102", "2016-01-22", "102"),
                       Fare("BF103", "2016-01-22", "103"),
                       Fare("BF104", "2016-01-22", "104"),
                       Fare("BF105", "2016-01-22", "105"),
                       Fare("BF106", "2016-01-22", "106"))

    repository.save(fares)

    logger.info { "Result: " + repository.getFareByFlightNumberAndFlightDate("BF101", "2016-01-22") }
  }
}

fun main(vararg args: String) {
  SpringApplication.run(Application::class.java, *args)
}