/*
 * Copyright (c) 2017-2020 by Coupang. Some rights reserved.
 * See the project homepage at: http://gitlab.coupang.net
 * Licensed under the Coupang License;
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://coupang.net/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.debop4k.pss.fares.component

import mu.KLogging
import org.debop4k.pss.fares.entity.Fare
import org.debop4k.pss.fares.repository.FaresRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * org.debop4k.pss.fares.component.FaresComponent
 * @author debop
 * @since 17. 10. 24
 */
@Component
class FaresComponent @Autowired constructor(private val repository: FaresRepository) {

  companion object : KLogging()

  fun getFare(flightNumber: String, flightDate: String): Fare? {
    logger.debug { "Looking for fares flightNumber=$flightNumber, flightDate=$flightDate" }
    return repository.getFareByFlightNumberAndFlightDate(flightNumber, flightDate)
  }

}